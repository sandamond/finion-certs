# Check if Chocolatey is installed
if (-not (Get-Command choco -ErrorAction SilentlyContinue)) {
    Write-Host "Chocolatey is not installed. Installing Chocolatey..."

    # Install Chocolatey
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]'Tls,Tls11,Tls12'; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    
    Write-Host "Chocolatey installed successfully."
}

# Check if Git is installed using Chocolatey
if (-not (Get-Command git -ErrorAction SilentlyContinue)) {
    Write-Host "Git is not installed. Installing Git using Chocolatey..."

    # Install Git using Chocolatey
    choco install git -y

    Write-Host "Git installed successfully."
# URL for the repository with certificates
$RepoUrl = "https://gitlab.com/sandamond/finion-certs.git"
# Name of the root certificate file
$RootCertName = "Finion-Root-CA.crt"
# Name of the intermediate certificate file
$IntermediateCertName = "Finion-Intermediate-CA.cer"
# Temporary directory for downloading certificates
$TempDir = [System.IO.Path]::Combine([System.IO.Path]::GetTempPath(), "finion_certs")
# Create a temporary directory
if (-not (Test-Path -Path $TempDir -PathType Container)) {
    New-Item -Path $TempDir -ItemType Directory
# Clone the repository with certificates
git clone $RepoUrl $TempDir

# Path to the root certificate
$RootCertPath = Join-Path -Path $TempDir -ChildPath $RootCertName

# Path to the intermediate certificate
$IntermediateCertPath = Join-Path -Path $TempDir -ChildPath $IntermediateCertName

# Add the root certificate to the system certificate store
Write-Host "Adding the root certificate to the system store"
$RootCert = Get-Item $RootCertPath
$RootCert | Import-Certificate -CertStoreLocation Cert:\LocalMachine\Root

# Add the intermediate certificate to the system certificate store
Write-Host "Adding the intermediate certificate to the system store"
$IntermediateCert = Get-Item $IntermediateCertPath
$IntermediateCert | Import-Certificate -CertStoreLocation Cert:\LocalMachine\CA

# Remove the temporary directory
Remove-Item -Path $TempDir -Force -Recurse

Write-Host "Installation completed"


# Check if ZeroTier One is in auto-start
$registryKey = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Run"
if (-not (Test-Path -Path $registryKey)) {
    Write-Host "Adding ZeroTier One to auto-start..."
    $ZeroTierPath = "C:\Program Files (x86)\ZeroTier\One\zerotier-cli.bat"
    New-ItemProperty -Path $registryKey -Name "ZeroTier One" -Value $ZeroTierPath -PropertyType String
} else {
    Write-Host "ZeroTier One is already in auto-start."
}

# Set ZeroTier to allow DNS configuration
Write-Host "Setting ZeroTier to allow DNS configuration..."
$ZerotierCliPath = "C:\Program Files (x86)\ZeroTier\One\zerotier-cli.bat"
& $ZerotierCliPath set 47002098a701be66 allowDNS=1

# Join ZeroTier network using zerotier-cli
Write-Host "Joining ZeroTier network..."
Start-Process -FilePath $ZerotierCliPath -ArgumentList "join 47002098a701be66" -Wait

# Start ZeroTier One GUI
Write-Host "Starting ZeroTier One GUI..."
$ZeroTierPath = "C:\Program Files (x86)\ZeroTier\One\zerotier_desktop_ui.exe"
Start-Process -FilePath $ZeroTierPath

# Wait for a moment for ZeroTier to connect
Start-Sleep -Seconds 5

# Get and display ZeroTier status
Write-Host "Getting ZeroTier status..."
$ZeroTierStatus = & $ZerotierCliPath status

if ($ZeroTierStatus -ne $null) {
    Write-Host -ForegroundColor Red -BackgroundColor Black "ZeroTier Status:"
    Write-Host $ZeroTierStatus
} else {
    Write-Host "Failed to retrieve ZeroTier status."
}

Write-Host "Installation and configuration completed."
