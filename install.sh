#!/bin/bash

# URL для репозитория с сертификатами
REPO_URL="https://gitlab.com/sandamond/finion-certs.git"

# Имя файла корневого сертификата
ROOT_CERT_NAME="Finion-Root-CA.crt"

# Имя файла промежуточного сертификата
INTERMEDIATE_CERT_NAME="Finion-Intermediate-CA.cer"

# Временная директория для загрузки сертификатов
TEMP_DIR="/tmp/finion_certs"

# Путь к системному хранилищу ключей
SYSTEM_KEYCHAIN="/Library/Keychains/System.keychain"

# Клонирование репозитория с сертификатами
git clone "$REPO_URL" "$TEMP_DIR"

# Путь к корневому сертификату
ROOT_CERT="$TEMP_DIR/$ROOT_CERT_NAME"

# Путь к промежуточному сертификату
INTERMEDIATE_CERT="$TEMP_DIR/$INTERMEDIATE_CERT_NAME"

# Добавление корневого сертификата в системное хранилище
sudo security add-trusted-cert -d -r trustRoot -k "$SYSTEM_KEYCHAIN" "$ROOT_CERT"

# Добавление промежуточного сертификата в системное хранилище
sudo security add-trusted-cert -d -r trustRoot -k "$SYSTEM_KEYCHAIN" "$INTERMEDIATE_CERT"

# Удаление временной директории
rm -rf "$TEMP_DIR"

echo "Установка завершена"


# Установка ZeroTier One

# URL для скачивания пакета ZeroTier One
ZEROTIER_DOWNLOAD_URL="https://download.zerotier.com/dist/ZeroTier%20One.pkg"

# Имя файла для скачивания
ZEROTIER_PKG="ZeroTier_One.pkg"

# Загрузка пакета
curl -o $ZEROTIER_PKG $ZEROTIER_DOWNLOAD_URL

# Установка ZeroTier One
sudo installer -pkg $ZEROTIER_PKG -target /

#подождем
sleep 5

# Присоединение к сети 47002098a701be66
sudo zerotier-cli join 47002098a701be66


# Разрешение DNS для сети
sudo zerotier-cli set 47002098a701be66 allowDNS=1

sudo zerotier-cli info

# Удаление скачанного пакета
rm $ZEROTIER_PKG
